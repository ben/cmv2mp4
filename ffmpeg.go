package main

import (
	"strconv"
	"time"
)

func (d *Decoder) ffmpegArgs() []string {
	args := []string{
		"-r", "100/" + strconv.Itoa(int(d.minRate)),
		"-f", "rawvideo",
		"-s", strconv.Itoa(int(d.maxCols)*10) + "x" + strconv.Itoa(int(d.maxRows)*12),
		"-pix_fmt", "rgba",
		"-i", "-",
	}

	if !*flagNoAudio && len(d.sounds) != 0 {
		var filter []byte
		for i, s := range d.sounds {
			filter = append(filter, '[')
			filter = strconv.AppendInt(filter, int64(i+1), 10)
			filter = append(filter, ":a]"...)
			if ms := s.delay / time.Millisecond; ms != 0 {
				filter = append(filter, "adelay="...)
				filter = strconv.AppendInt(filter, int64(ms), 10)
				filter = append(filter, '|')
				filter = strconv.AppendInt(filter, int64(ms), 10)
				filter = append(filter, ',')
			}
			filter = append(filter, "apad[a"...)
			filter = strconv.AppendInt(filter, int64(i), 10)
			filter = append(filter, "];"...)
			args = append(args, "-i", s.name+".ogg")
		}

		for i := range d.sounds {
			filter = append(filter, "[a"...)
			filter = strconv.AppendInt(filter, int64(i), 10)
			filter = append(filter, ']')
		}

		filter = append(filter, "amerge=inputs="...)
		filter = strconv.AppendInt(filter, int64(len(d.sounds)), 10)
		filter = append(filter, ",pan=stereo|c0="...)
		for i := range d.sounds {
			if i != 0 {
				filter = append(filter, '+')
			}
			filter = append(filter, 'c')
			filter = strconv.AppendInt(filter, int64(i*2), 10)
		}
		filter = append(filter, "|c1="...)
		for i := range d.sounds {
			if i != 0 {
				filter = append(filter, '+')
			}
			filter = append(filter, 'c')
			filter = strconv.AppendInt(filter, int64(i*2+1), 10)
		}
		filter = append(filter, "[aout]"...)

		args = append(args,
			"-filter_complex", string(filter),
			"-map", "0:v",
			"-map", "[aout]",
			"-c:a", "aac",
			"-shortest",
		)
	}

	preset := "fast"
	if *flagSlow {
		preset = "veryslow"
	}

	return append(args,
		"-c:v", "libx264",
		"-preset", preset,
		"-crf", "18",
		"-pix_fmt", "yuv420p",
		"-movflags", "+faststart",
		"-r", "50",
		"-y", *flagOutput,
	)
}
