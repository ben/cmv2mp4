package main

import (
	"errors"
	"strconv"
)

type errInvalidVersion uint32

func (err errInvalidVersion) Error() string {
	return "cmv2mp4: invalid version in header: " + strconv.FormatUint(uint64(err), 10)
}

var (
	errChunkTooBig  = errors.New("cmv2mp4: chunk too big")
	errSizeMismatch = errors.New("cmv2mp4: resolution mismatch")
	errRateMismatch = errors.New("cmv2mp4: frame rate mismatch")
	errNextSounds   = errors.New("cmv2mp4: continuation files may not contain audio")
)
