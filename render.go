package main

type frame struct {
	number int
	in     []uint8
	out    []uint32
}

// draw 3 rows of a tile (one quarter of a full tile)
func draw3(buf []uint32, offset, stride int, fg1, fg2, bg, map1, map2 uint32) {
	for y := 0; y < 3; y++ {
		for x := 0; x < 10; x++ {
			bit := uint32(1) << x

			if map1&bit == 0 {
				buf[x+offset] = bg
			} else if map2&bit == 0 {
				buf[x+offset] = fg1
			} else {
				buf[x+offset] = fg2
			}
		}

		offset += stride

		map1 >>= 10
		map2 >>= 10
	}
}

func drawTile(buf []uint32, offset, stride int, ch, attr uint8) {
	fg := attr&7 | (attr>>6)<<3
	fg1 := palette[fg]
	fg2 := palette[fg|16]
	bg := palette[(attr>>3)&7]

	c1 := &curses_charmap[ch]
	c2 := &curses_empty
	if ch == 1 || ch == 2 {
		c2 = &curses_face
	}

	stride3 := stride * 3

	draw3(buf, offset, stride, fg1, fg2, bg, c1[0], c2[0])
	offset += stride3
	draw3(buf, offset, stride, fg1, fg2, bg, c1[1], c2[1])
	offset += stride3
	draw3(buf, offset, stride, fg1, fg2, bg, c1[2], c2[2])
	offset += stride3
	draw3(buf, offset, stride, fg1, fg2, bg, c1[3], c2[3])
}

func drawFrame(in []uint8, out []uint32, width, height, stride int) {
	attr := width * height
	row := stride * 12
	offset := 0

	for y := 0; y < height; y++ {
		i := y

		for x := 0; x < width; x++ {
			drawTile(out, offset+x*10, stride, in[i], in[i+attr])
			i += height
		}

		offset += row
	}
}
