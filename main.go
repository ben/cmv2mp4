package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"unsafe"
)

var (
	flagOutput  = flag.String("o", "", "(required) output mp4 file")
	flagNoAudio = flag.Bool("an", false, "no audio")
	flagSlow    = flag.Bool("slow", false, "use slower but better compression")
	flagGetCmd  = flag.Bool("get-cmd", false, "instead of encoding a video, print the ffmpeg command that would have been run")
	flagRaw     = flag.Bool("raw", false, "instead of encoding a video, write raw RGBA video data to standard output")
)

func main() {
	flag.Parse()

	if *flagOutput == "" || flag.NArg() < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var d Decoder
	defer d.Close()

	for _, name := range flag.Args() {
		f, err := os.Open(name)
		if err != nil {
			panic(err)
		}

		err = d.Pass1(f)
		if err != nil {
			panic(err)
		}

		err = f.Close()
		if err != nil {
			panic(err)
		}
	}

	args := d.ffmpegArgs()

	if *flagGetCmd {
		fmt.Println("ffmpeg", strings.Join(args, " "))
		return
	}

	if *flagRaw {
		encodeRawVideo(&d, os.Stdout)
		return
	}

	ffmpeg := exec.Command("ffmpeg", args...)
	ffmpeg.Stdout = os.Stdout
	ffmpeg.Stderr = os.Stderr
	pipe, err := ffmpeg.StdinPipe()
	if err != nil {
		panic(err)
	}

	err = ffmpeg.Start()
	if err != nil {
		panic(err)
	}

	encodeRawVideo(&d, pipe)

	if err := pipe.Close(); err != nil {
		panic(err)
	}

	if err := ffmpeg.Wait(); err != nil {
		panic(err)
	}
}

func encodeRawVideo(d *Decoder, pipe io.Writer) {
	for _, name := range flag.Args() {
		f, err := os.Open(name)
		if err != nil {
			panic(err)
		}

		err = d.Init(f)
		if err != nil {
			panic(err)
		}

		frameBytes := (*[maxUncompressed * 10 * 12]byte)(unsafe.Pointer(&d.Frame[0]))[: len(d.Frame)*4 : len(d.Frame)*4]

		for {
			err := d.NextFrame()
			if err == io.EOF {
				break
			}
			if err != nil {
				panic(err)
			}

			_, err = pipe.Write(frameBytes)
			if err != nil {
				panic(err)
			}

			runtime.KeepAlive(d.Frame)
		}
	}
}
