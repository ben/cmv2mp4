package main

import "testing"

func BenchmarkDrawSprite(b *testing.B) {
	const char = 1
	const attr = 7
	const stride = 10
	var buf [10 * 12]uint32

	b.SetBytes(10 * 12 * 4)

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			drawTile(buf[:], 0, stride, char, attr)
		}
	})
}
