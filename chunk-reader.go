package main

import (
	"compress/zlib"
	"encoding/binary"
	"io"
)

const maxCompressed = 1000000
const maxUncompressed = 800000

type zreader interface {
	io.ReadCloser
	zlib.Resetter
}

// ChunkReader is a decoder for Dwarf Fortress's chunked zlib format.
type ChunkReader struct {
	zr  zreader
	buf [maxUncompressed]byte
}

// Next reads and returns the next chunk from the file.
// The slice returned by Next is invalid after the next call to Next.
func (c *ChunkReader) Next(r io.Reader) ([]byte, error) {
	var size uint32
	err := binary.Read(r, binary.LittleEndian, &size)
	if err != nil {
		return nil, err
	}

	if size > maxCompressed {
		return nil, errChunkTooBig
	}

	lr := io.LimitReader(r, int64(size))
	if c.zr == nil {
		zr, err := zlib.NewReader(lr)
		if err != nil {
			return nil, err
		}

		c.zr = zr.(zreader)
	} else {
		err = c.zr.Reset(lr, nil)
		if err != nil {
			return nil, err
		}
	}

	n := 0
	for {
		i, err := c.zr.Read(c.buf[n:])
		n += i

		if err == io.EOF {
			return c.buf[:n], nil
		}

		if err != nil {
			return c.buf[:n], err
		}
	}
}

func (c *ChunkReader) Close() error {
	if c.zr != nil {
		return c.zr.Close()
	}

	return nil
}
