package main

import (
	"encoding/binary"
	"io"
)

const (
	cmv0 = 10000
	cmv1 = 10001
)

type Header struct {
	BasicHeader
	Sounds *Sounds
}

type BasicHeader struct {
	Version   uint32
	Columns   uint32
	Rows      uint32
	DelayRate uint32
}

type Sounds struct {
	Files [][50]byte
	Time  [200][16]int32
}

func (h *Header) ReadFrom(r io.Reader) (n int64, err error) {
	err = binary.Read(r, binary.LittleEndian, &h.BasicHeader)
	if err != nil {
		return 0, err
	}

	switch h.Version {
	case 10000:
		return 16, nil
	case 10001:
		var count uint32
		err = binary.Read(r, binary.LittleEndian, &count)
		if err == io.EOF {
			return 16, io.ErrUnexpectedEOF
		}
		if err != nil {
			return 16, err
		}

		h.Sounds = &Sounds{
			Files: make([][50]byte, count),
		}

		err = binary.Read(r, binary.LittleEndian, &h.Sounds.Files)
		if err == io.EOF {
			return 20, io.ErrUnexpectedEOF
		}
		if err != nil {
			return 20, err
		}

		err = binary.Read(r, binary.LittleEndian, &h.Sounds.Time)
		if err == io.EOF {
			return 20 + 50*int64(count), io.ErrUnexpectedEOF
		}
		if err != nil {
			return 20 + 50*int64(count), err
		}

		if *flagNoAudio {
			h.Sounds = nil
		}

		return 12820 + 50*int64(count), nil
	default:
		return 16, errInvalidVersion(h.Version)
	}
}

func (h *Header) Compatible(next *Header) error {
	if h.Columns != next.Columns || h.Rows != next.Rows {
		return errSizeMismatch
	}

	rate1 := h.DelayRate
	rate2 := next.DelayRate
	if rate1 == 0 {
		rate1 = 2
	}
	if rate2 == 0 {
		rate2 = 2
	}
	if rate1 != rate2 {
		return errRateMismatch
	}

	if next.Sounds != nil && len(next.Sounds.Files) != 0 {
		for frame := range next.Sounds.Time {
			for _, id := range next.Sounds.Time[frame] {
				if id != -1 {
					return errNextSounds
				}
			}
		}
	}

	return nil
}
