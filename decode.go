package main

import (
	"bytes"
	"io"
	"time"
)

type Decoder struct {
	Header *Header
	Frame  []uint32
	r      io.ReadCloser
	cr     ChunkReader
	read   []uint8
	cols   int
	rows   int
	size   int
	stride int

	maxCols    uint32
	maxRows    uint32
	minRate    uint32
	skip       uint32
	delayRate  uint32
	totalDelay time.Duration
	sounds     []queuedSound
}

type queuedSound struct {
	name    string
	delay   time.Duration
	channel int
}

func (d *Decoder) Pass1(r io.Reader) error {
	var h Header
	if _, err := h.ReadFrom(r); err != nil {
		return err
	}

	if d.maxCols < h.Columns {
		d.maxCols = h.Columns
	}
	if d.maxRows < h.Rows {
		d.maxRows = h.Rows
	}

	delayRate := h.DelayRate
	if delayRate == 0 {
		delayRate = 2
	}
	if d.minRate > delayRate || d.minRate == 0 {
		d.minRate = delayRate
	}

	frameDelay := time.Duration(delayRate) * time.Second / 100

	if h.Sounds != nil && len(h.Sounds.Files) != 0 {
		names := make([]string, len(h.Sounds.Files))
		for i := range h.Sounds.Files {
			b := h.Sounds.Files[i][:]
			names[i] = string(b[:bytes.IndexByte(b, 0)])
		}

		for frame := range h.Sounds.Time {
			for channel, id := range h.Sounds.Time[frame] {
				if id == -1 {
					continue
				}

				d.sounds = append(d.sounds, queuedSound{
					name:    names[id],
					delay:   d.totalDelay + time.Duration(frame)*frameDelay,
					channel: channel,
				})
			}
		}
	}

	frameSize := int(h.Columns) * int(h.Rows) * 2

	for {
		b, err := d.cr.Next(r)
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		d.totalDelay += time.Duration(len(b)/frameSize) * frameDelay
	}

	return nil
}

func (d *Decoder) Init(r io.ReadCloser) error {
	if d.r != nil {
		err := d.r.Close()
		if err != nil {
			return err
		}
	}

	var h Header
	if _, err := h.ReadFrom(r); err != nil {
		return err
	}

	if d.maxCols != 0 {
		d.cols = int(h.Columns)
		d.rows = int(h.Rows)
		d.size = d.cols * d.rows * 2
		d.skip = 0
		d.delayRate = h.DelayRate
		if d.delayRate == 0 {
			d.delayRate = 2
		}
		if d.Frame == nil {
			d.stride = int(d.maxCols) * 10
			d.Frame = make([]uint32, 10*12*d.maxCols*d.maxRows)
		} else {
			for i := range d.Frame {
				d.Frame[i] = 0
			}
		}
	} else if d.Header == nil {
		d.cols = int(h.Columns)
		d.rows = int(h.Rows)
		d.size = d.cols * d.rows * 2
		d.stride = d.cols * 10
		d.Frame = make([]uint32, 10*12*h.Columns*h.Rows)
	} else {
		err := d.Header.Compatible(&h)
		if err != nil {
			return err
		}
	}

	d.Header = &h
	d.r = r
	d.read = nil

	return nil
}

func (d *Decoder) Close() error {
	var err1, err2 error
	err1 = d.cr.Close()
	if d.r != nil {
		err2 = d.r.Close()
	}

	if err1 != nil {
		return err1
	}
	return err2
}

func (d *Decoder) NextFrame() error {
	if d.minRate != 0 && d.minRate != d.delayRate {
		shouldRender := d.skip < d.minRate
		if shouldRender {
			d.skip += d.delayRate
		}
		d.skip -= d.minRate
		if !shouldRender {
			return nil
		}
	}

	if len(d.read) == 0 {
		buf, err := d.cr.Next(d.r)
		if err != nil {
			return err
		}

		d.read = buf
	}

	drawFrame(d.read[:d.size], d.Frame, d.cols, d.rows, d.stride)
	d.read = d.read[d.size:]
	return nil
}
